Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: micropython
Upstream-Contact: Damien P. George <damien.p.george@gmail.com>
Source: https://github.com/bbcmicrobit/micropython
Files-Excluded:
 .gitignore
Comment: The following files were removed:
 .gitignore : ignore upstream git configuration

Files: *
Copyright: 2013-16 The MicroPython-on-micro:bit Developers, as listed in the
                   accompanying AUTHORS file
License: Expat
Comment: This is the project-wide statement as found in LICENSE. Expat-licensed
 files with copyright statements from author(s) *not* listed below (taken from
 AUTHORS) are shown separately below.
 .
 Damien P. George (@dpgeorge)
 Nicholas H. Tollervey (@ntoll)
 Matthew Else (@matthewelse)
 Alan M. Jackson (@alanmjackson)
 Mark Shannon (@markshannon)
 Larry Hastings (@larryhastings)
 Mariia Koroliuk (@marichkakorolyuk)
 Andrew Mulholland (@gbaman)
 Joe Glancy (@JoeGlancy)

Files: examples/flame_simulation.py
Copyright: n/a
License: public-domain
Comment: Author: M. Schafer

Files: examples/magic8.py
       examples/radio.py
Copyright: n/a
License: public-domain
Comment: Author: Nicholas Tollervey

Files: examples/simple_slalom.py
Copyright: n/a
License: public-domain
Comment: Author: Larry Hastings

Files: examples/tiltmusic.py
Copyright: n/a
License: public-domain
Comment: Author: Alex "Chozabu" P-B

Files: inc/extmod/utime_mphal.h
       inc/py/objarray.h
       source/extmod/utime_mphal.c
       source/microbit/modrandom.cpp
       source/py/bc.c
       source/py/builtinimport.c
       source/py/frozenmod.c
       source/py/modstruct.c
       source/py/modsys.c
       source/py/objarray.c
       source/py/objfun.c
       source/py/objgenerator.c
       source/py/objint_longlong.c
       source/py/objnamedtuple.c
       source/py/objstr.c
       source/py/objstringio.c
       source/py/objstrunicode.c
       source/py/objtype.c
       source/py/sequence.c
       source/py/stream.c
       source/py/vm.c
Copyright: 2013-16 Damien P. George
           2014-17 Paul Sokolovsky
License: Expat
Comment: Paul Sokolovsky not listed in AUTHORS

Files: inc/microbit/microbit_image.h
Copyright: 2015 Mark Shannon
           2015-2017 Damien P. George
License: Expat
Comment: Copyright years outside blanket statement in LICENSE

Files: inc/microbit/microbitdal.h
       inc/microbit/modmicrobit.h
       inc/microbit/modmusic.h
       inc/microbit/mpconfigport.h
       source/microbit/events.cpp
       source/microbit/gccollect.c
       source/microbit/microbiti2c.cpp
       source/microbit/microbitspi.cpp
       source/microbit/modmachine.c
       source/microbit/modutime.c
       source/py/nlrx64.c
       source/py/nlrx86.c
       source/py/nlrxtensa.c
       source/py/scheduler.c
Copyright: 2013-17 Damien P. George
License: Expat
Comment: Copyright years outside blanket statement in LICENSE

Files: inc/py/asmarm.h
       source/py/asmarm.c
Copyright: 2014 Fabian Vogt
           2013-14 Damien P. George
License: Expat
Comment: Fabian Vogt not listed in AUTHORS

Files: inc/py/mpthread.h
       source/py/modthread.c
Copyright: 2016 Damien P. George on behalf of Pycom Ltd
License: Expat

Files: inc/py/ringbuf.h
       inc/py/stackctrl.h
       source/py/objpolyiter.c
       source/py/stackctrl.c
Copyright: 2014-16 Paul Sokolovsky
License: Expat
Comment: Paul Sokolovsky not listed in AUTHORS

Files: source/lib/neopixelsend.s
Copyright: Microsoft Corporation
License: Expat
Comment:
 Portions of this code based on code provided under MIT license from Microsoft
 https://github.com/Microsoft/pxt-ws2812b

Files: source/lib/pwm.c
Copyright: 2016 Mark Shannon
           2017 Damien P. George
License: Expat
Comment: Copyright years outside blanket statement in LICENSE

Files: source/py/runtime_utils.c
Copyright: 2015 Josef Gajdusek
           2015 Paul Sokolovsky
License: Expat
Comment: Neither Josef Gajdusek nor Paul Sokolovsky listed in AUTHORS

Files: yotta-modules/ble/*
Copyright: 2006-15 ARM Limited
License: Apache-2.0
Comment: Imported into source tree as part of yotta-modules source tarball.
 Upstream repository: https://github.com/ARMmbed/ble

Files: yotta-modules/ble-nrf51822/*
Copyright: 2006-15 ARM Limited
License: Apache-2.0
Comment: Imported into source tree as part of yotta-modules source tarball.
 Upstream repository: https://github.com/ARMmbed/ble-nRF51822
 .
 yotta-modules/ble-nrf51822/bootloader/ is manually removed prior to creating
 the additional yotta-modules component tarball as it contains a precompiled
 bootloader that is not required for compilation.

Files: yotta-modules/ble-nrf51822/source/common/ansi_escape.h
Copyright: 2013 hathach (tinyusb.org)
License: BSD-3-Clause

Files: yotta-modules/ble-nrf51822/source/common/assertion.h
       yotta-modules/ble-nrf51822/source/common/binary.h
       yotta-modules/ble-nrf51822/source/common/ble_error.h
       yotta-modules/ble-nrf51822/source/common/common.h
       yotta-modules/ble-nrf51822/source/common/compiler.h
Copyright: 2013 K. Townsend (microBuilder.eu)
License: BSD-3-Clause

Files: yotta-modules/mbed-classic/*
Copyright: 2006-15 ARM Limited
License: Apache-2.0
Comment: Imported into source tree as part of yotta-modules source tarball.
 Upstream repository: https://github.com/mbedmicro/mbed

Files: yotta-modules/mbed-classic/targets/cmsis/*
Copyright: 2009-15 ARM Limited
License: BSD-3-Clause-Arm

Files: yotta-modules/mbed-classic/targets/cmsis/TARGET_NORDIC/TARGET_MCU_NRF51822/TOOLCHAIN_GCC_ARM/startup_NRF51822.S
       yotta-modules/mbed-classic/targets/cmsis/TARGET_NORDIC/TARGET_MCU_NRF51822/compiler_abstraction.h
       yotta-modules/mbed-classic/targets/cmsis/TARGET_NORDIC/TARGET_MCU_NRF51822/nrf51_bitfields.h
       yotta-modules/mbed-classic/targets/cmsis/TARGET_NORDIC/TARGET_MCU_NRF51822/nrf51.h
       yotta-modules/mbed-classic/targets/cmsis/TARGET_NORDIC/TARGET_MCU_NRF51822/nrf.h
       yotta-modules/mbed-classic/targets/cmsis/TARGET_NORDIC/TARGET_MCU_NRF51822/system_nrf51.c
       yotta-modules/mbed-classic/targets/cmsis/TARGET_NORDIC/TARGET_MCU_NRF51822/system_nrf51.h
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/Lib/nordic_sdk/*
Copyright: 2012-13 Nordic Semiconductor ASA
License: BSD-3-Clause-Nordic

Files: yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/gpio_irq_api.c
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/i2c_api.c
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/objects.h
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/PeripheralNames.h
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/port_api.c
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/PortNames.h
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/pwmout_api.c
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/serial_api.c
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/spi_api.c
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/twi_config.h
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/twi_master.c
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/twi_master.h
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/us_ticker.c
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/TARGET_ARCH_BLE/PinNames.h
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/TARGET_DELTA_DFCM_NNN40/PinNames.h
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/TARGET_HRM1017/PinNames.h
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/TARGET_NRF51822_MKIT/PinNames.h
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/TARGET_NRF51822_SBKIT/PinNames.h
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/TARGET_NRF51822_Y5_MBUG/PinNames.h
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/TARGET_NRF51_DK/PinNames.h
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/TARGET_NRF51_DONGLE/PinNames.h
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/TARGET_NRF51_MICROBIT/PinNames.h
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/TARGET_RBLAB_BLENANO/PinNames.h
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/TARGET_RBLAB_NRF51822/PinNames.h
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/TARGET_SEEED_TINY_BLE/PinNames.h
       yotta-modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/TARGET_WALLBOT_BLE/PinNames.h
Copyright: 2009-15 Nordic Semiconductor
License: Apache-2.0

Files: yotta-modules/microbit-dal/*
Copyright: 2016 British Broadcasting Corporation
License: Expat
Comment: Imported into source tree as part of yotta-modules source tarball.
 Upstream repository: https://github.com/lancaster-university/microbit-dal

Files: yotta-modules/microbit-dal/inc/core/MicroBitUtil.h
       yotta-modules/microbit-dal/inc/drivers/FXOS8700.h
       yotta-modules/microbit-dal/inc/drivers/LSM303Accelerometer.h
       yotta-modules/microbit-dal/inc/drivers/LSM303Magnetometer.h
       yotta-modules/microbit-dal/inc/drivers/MAG3110.h
       yotta-modules/microbit-dal/inc/drivers/MicroBitAccelerometer.h
       yotta-modules/microbit-dal/inc/drivers/MicroBitCompass.h
       yotta-modules/microbit-dal/inc/drivers/MMA8653.h
       yotta-modules/microbit-dal/inc/drivers/TimedInterruptIn.h
       yotta-modules/microbit-dal/inc/types/CoordinateSystem.h
       yotta-modules/microbit-dal/source/core/MicroBitUtil.cpp
       yotta-modules/microbit-dal/source/drivers/FXOS8700.cpp
       yotta-modules/microbit-dal/source/drivers/LSM303Accelerometer.cpp
       yotta-modules/microbit-dal/source/drivers/LSM303Magnetometer.cpp
       yotta-modules/microbit-dal/source/drivers/MAG3110.cpp
       yotta-modules/microbit-dal/source/drivers/MicroBitAccelerometer.cpp
       yotta-modules/microbit-dal/source/drivers/MicroBitCompass.cpp
       yotta-modules/microbit-dal/source/drivers/MMA8653.cpp
       yotta-modules/microbit-dal/source/drivers/TimedInterruptIn.cpp
       yotta-modules/microbit-dal/source/types/CoordinateSystem.cpp
Copyright: 2016-17 Lancaster University
License: Expat

Files: yotta-modules/microbit-dal/inc/drivers/MicroBitQuadratureDecoder.h
       yotta-modules/microbit-dal/source/drivers/MicroBitQuadratureDecoder.cpp
Copyright: 2016-17 Simon Hosie
License: Expat

Files: yotta-modules/nrf51-sdk/*
Copyright: 2015 ARM Limited
License: Apache-2.0
Comment: Imported into source tree as part of yotta-modules source tarball.
 Upstream repository: https://github.com/ARMmbed/nrf51-sdk

Files: yotta-modules/nrf51-sdk/source/nordic_sdk/*
Copyright: Nordic Semiconductor ASA
License: BSD-3-Clause-Nordic

Files: yotta-targets/*
Copyright: 2014-15 ARM Limited
License: Apache-2.0
Comment: Imported into source tree as part of yotta-targets source tarball.

Files: debian/*
Copyright: 2018-2023 Nick Morrott <nickm@debian.org>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     https://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the full text of the Apache License version 2.0
 can be found in the file `/usr/share/common-licenses/Apache-2.0'.

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 3. Neither the name of the copyright holders nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: BSD-3-Clause-Nordic
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 .
 3. Neither the name of Nordic Semiconductor ASA nor the names of other
    contributors to this software may be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-Clause-Arm
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 .
 3. Neither the name of ARM Limited nor the names of other contributors to this
    software may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: public-domain
 Files tagged with this license contains the following:
 .
 This program has been placed into the public domain.
