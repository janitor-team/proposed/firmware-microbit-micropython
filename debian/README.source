firmware-microbit-micropython
=============================

The firmware-microbit-micropython package is compiled from source using the
ARMmbed yotta build system, which requires an internet connection for initial
dependency resolution (cf. apt-get). The `yotta update` command downloads
modules present in the dependency graph for the project (top-level dependencies
for a yotta-based project are listed in module.json)

In order to allow this package to build without internet access, snapshots of
these required dependencies are made available via the source tarball
components "yotta-modules" and "yotta-targets". These tarball snapshots are
generated by running a fresh yotta update/build on a host with internet access,
and then filtering the downloaded files to remove as many unnecessary files
as possible.


yotta build dependencies
------------------------

As part of a fresh build of MicroPython for the micro:bit, the `yotta update`
step downloads all configured yotta dependencies into:

    yotta_modules/


yotta build targets
-------------------

When setting a build target, the chosen target (and any dependency targets)
is downloaded into:

    yotta_targets/


Removed files
-------------

The following files/directories are removed from the source tarball components:

$ rm -rf yotta_modules/ble-nrf51822/bootloader/
$ rm -rf yotta_modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/Lib/s110_nrf51822_8_0_0
$ rm -rf yotta_modules/mbed-classic/targets/hal/TARGET_NORDIC/TARGET_MCU_NRF51822/Lib/s130_nrf51822_1_0_0

$ cd yotta-modules/mbed-classic/targets/

$ find -type d -name TOOLCHAIN_IAR -exec rm -rf '{}' \;
$ find -type d -name TOOLCHAIN_ARM_STD -exec rm -rf '{}' \;
$ find -type d -name TOOLCHAIN_ARM_MICRO -exec rm -rf '{}' \;

$ cd cmsis
$ rm -rf TARGET_ARM_SSG TARGET_Atmel TARGET_Freescale TARGET_Maxim TARGET_NXP TARGET_RENESAS TARGET_Silicon_Labs TARGET_STM TARGET_WIZNET

$ cd ../hal
$ rm -rf TARGET_ARM_SSG TARGET_Atmel TARGET_Freescale TARGET_Maxim TARGET_NXP TARGET_RENESAS TARGET_Silicon_Labs TARGET_STM TARGET_WIZNET


Source tarballs
---------------

After pruning the above files from the freshly yotta-updated tree, the source
tarball components are created in the root project directory. E.g. for upstream
version 1.0.1:

$ tar cJf firmware-microbit-micropython_1.0.1.orig-yotta-modules.tar.xz -C ../micropython yotta_modules
$ tar cJf firmware-microbit-micropython_1.0.1.orig-yotta-targets.tar.xz -C ../micropython yotta_targets

(adjust version to match upstream firmware-microbit-micropython version)


git-buildpackage: importing new tarballs
----------------------------------------

The source components are managed in git-buildpackage using the "--component"
support in `gbp import-orig`, e.g. (for upstream version 1.0.1):

$ gbp import-orig --pristine-tar \
    --component=yotta-modules --component=yotta-targets \
    ../firmware-microbit-micropython_1.0.1.orig.tar.xz


git-buildpackage: building packages
-----------------------------------

When using git-buildpackage to build the binary packages via `gbp buildpackage`
we must ensure that the additional upstream component tarballs are generated.

debian/gbp.conf is configured to do this, and to use pristine-tar.

$ gbp buildpackage

E.g. for upstream version 1.0.1, the following tarballs should be created:

 - firmware-microbit-micropython_1.0.1.orig.tar.xz
 - firmware-microbit-micropython_1.0.1.orig-yotta-modules.tar.xz
 - firmware-microbit-micropython_1.0.1.orig-yotta-targets.tar.xz


Building the firmware
---------------------

debian/rules overrides dh_auto_build to call `make all` which creates a
freshly-built firmware.hex file in build/. This file is injected into the
firmware-microbit-micropython binary file and is installed to
/usr/share/firmware-microbit-micropython/firmware.hex for use by tools
such as uflash.


 -- Nick Morrott <knowledgejunkie@gmail.com>  Sat, 19 Jan 2019 03:34:00 +0000
